# simple server which starts server.py with needed params
from flask import Flask, jsonify, request

from collections import OrderedDict
import threading
import subprocess
import uuid

from config import Config

SERVER_HOST = Config.lobby_host.split(":")[0].strip()
LOBBY_PORT = int(Config.lobby_host.split(":")[-1].strip())
NUMBER_OF_PLAYERS = 4

MIN_PORT = 40000
BUSY_PORTS = set()

LOBBY_GAMES = OrderedDict()

def run_new_server(on_exit_callback, on_new_line_callback, port):
    def runInThread(proc, on_exit):
        proc.wait()
        on_exit()
        return
    def readInThread(proc, on_new_line):
        running = True
        while running:
            poll = proc.poll()
            if poll != None:
                return
            line = proc.stdout.readline()
            on_new_line(line)
    proc = subprocess.Popen(get_server_filename() + [str(NUMBER_OF_PLAYERS), str(port)], bufsize=0, stdout=subprocess.PIPE, universal_newlines=True)

    read_thread = threading.Thread(target=readInThread,
                                args=(proc, on_new_line_callback))
    thread = threading.Thread(target=runInThread,
                              args=(proc, on_exit_callback))
    read_thread.start()
    thread.start()


def get_server_filename():
    return ["python", "server.py"]


def next_free_port():
    if len(BUSY_PORTS) == 0:
        BUSY_PORTS.add(MIN_PORT)
        return MIN_PORT
    else:
        port = MIN_PORT
        for i in range(10):
            if port in BUSY_PORTS:
                port += 1
                continue
            else:
                BUSY_PORTS.add(port)
                return port
        return None

def free_port(port):
    BUSY_PORTS.remove(port)


app = Flask(__name__)

@app.route("/lobby/new", methods=['POST'])
def create_game():
    title = request.form.get('title')
    if title is None:
        return jsonify(msg="No title given"), 400

    game_id = str(uuid.uuid4())

    game_port = next_free_port()
    if game_port is None:
        return jsonify(msg=u'All slots are busy'), 503
    LOBBY_GAMES[game_id] = {'title': title, 'host': SERVER_HOST, 'port': game_port, 'players': 0, 'started': False}
    def exit_callback():
        free_port(game_port)
        del LOBBY_GAMES[game_id]
    def new_line_callback(line):
        if line.startswith("GAME STARTED"):
            LOBBY_GAMES[game_id]['started'] = True
        if line.startswith("PLAYERS:"):
            players_count = int(line.split("PLAYERS:")[-1].strip())
            LOBBY_GAMES[game_id]['players'] = players_count
    run_new_server(exit_callback, new_line_callback, game_port)
    return jsonify(LOBBY_GAMES[game_id])


@app.route("/lobby")
def lobby_list():
    if request.args.get('full'):
        result = LOBBY_GAMES
    else:
        result = {k:v for (k, v) in LOBBY_GAMES.items() if v['started'] == False}
    return jsonify(result)

if __name__ == '__main__':
    app.run(port=45001)
