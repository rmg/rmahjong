
from lobby_utils import get_lobby_games_list, create_new_game
from states import OfflineState, ConnectingState, StartServerState, ErrorState
from gui import Button, TextWidget, Label
import graphics
import pygame


class MenuState(OfflineState):

    def __init__(self, mahjong, label, items):
        OfflineState.__init__(self, mahjong)
        self.items = items
        self.label = label

    def enter_state(self):
        OfflineState.enter_state(self)
        x, y = 340, 250
        widgets = [TextWidget((x + 150, y), self.label,
                              font=graphics.font_large)]
        y += 40
        for name, callback in self.items:
            button = Button((x, y), (320, 40), name, callback)
            widgets.append(button)
            y += 50

        self.setup_widgets(widgets)


class EntryState(OfflineState):

    def __init__(self, mahjong, label, default="", limit=256):
        OfflineState.__init__(self, mahjong)
        self.label = label
        self.string = default
        self.limit = limit

    def enter_state(self):
        OfflineState.enter_state(self)
        x, y = 340, 250
        l1 = Label((200, 300), (600, 40), self.label)
        self.text = Label((200, 360), (600, 40), self.string + "|")
        self.setup_widgets([l1, self.text])

    def on_key_down(self, event):
        if event.key == pygame.K_BACKSPACE:
            if self.string:
                self.string = self.string[:-1]
        elif event.key == pygame.K_RETURN:
            self.on_accept(self.string)
        elif event.key == pygame.K_ESCAPE:
            self.on_escape()
        elif len(self.string) < self.limit:
            self.string += event.unicode
        self.text.update_text(self.string + "|")

    def on_accept(self, string):
        pass

    def on_escape(self):
        pass


class PlayerNameState(EntryState):

    def __init__(self, mahjong, accept_callback):
        EntryState.__init__(self, mahjong, _(
            "Player name") + ":", mahjong.get_username(), 18)
        self.accept_callback = accept_callback

    def on_accept(self, string):
        self.mahjong.set_username(string)
        self.accept_callback(self)

    def on_escape(self):
        self.mahjong.open_main_menu()


class AddressEntryState(EntryState):

    def __init__(self, mahjong):
        EntryState.__init__(self, mahjong, _(
            "Connect to the address")+":", "localhost")

    def on_accept(self, string):
        port = 4500
        host_parts = string.split(":", 1)
        if len(host_parts) == 2:
            hostname, port_str = host_parts
            if port_str.isdigit():
                port_num = int(port_str)
                if port_num > 0 and port_num <= 65535:
                    port = port_num
        else:
            hostname = string
        self.mahjong.set_state(ConnectingState(self.mahjong, hostname, port))

    def on_escape(self):
        self.mahjong.open_main_menu()


class PlayersCountState(MenuState):

    def __init__(self, mahjong):
        items = [(_("Singleplayer (1 human + 3 bots)"), self.on_run1),
                 (_("Multiplayer (2 human + 2 bots)"), self.on_run2),
                 (_("Multiplayer (3 human + 1 bot)"), self.on_run3),
                 (_("Multiplayer (4 humans)"), self.on_run4),
                 (_("Back to main menu"), self.on_main_menu),
                 ]
        MenuState.__init__(self, mahjong, _("Game type"), items)

    def on_main_menu(self, button):
        self.mahjong.open_main_menu()

    def on_run1(self, button):
        self.run_server(1)

    def on_run2(self, button):
        self.run_server(2)

    def on_run3(self, button):
        self.run_server(3)

    def on_run4(self, button):
        self.run_server(4)

    def run_server(self, number_of_players):
        self.mahjong.set_state(StartServerState(
            self.mahjong, number_of_players))


class MainMenuState(MenuState):
    def __init__(self, mahjong):
        items = [(_("New game"), self.on_new_game),
                 (_("Enter lobby"), self.on_open_lobby),
                 (_("Join to network game"), self.on_join_game),
                 (_("Options"), self.on_options),
                 (_("Quit"), self.on_quit)]
        MenuState.__init__(self, mahjong, "RMahjong", items)

    def enter_state(self):
        MenuState.enter_state(self)
        self.add_widget(TextWidget(
            (530, 275), "v" + self.mahjong.get_version_string(), font=graphics.font_small))

    def on_new_game(self, button):
        self.mahjong.set_state(PlayerNameState(
            self.mahjong, lambda state: state.mahjong.set_state(PlayersCountState(state.mahjong))))
    
    def on_open_lobby(self, button):
        self.mahjong.set_state(LobbyMenuState(self.mahjong))

    def on_join_game(self, button):
        self.mahjong.set_state(PlayerNameState(
            self.mahjong, lambda state: state.mahjong.set_state(AddressEntryState(state.mahjong))))

    def on_quit(self, button):
        self.mahjong.quit()

    def on_options(self, button):
        self.mahjong.set_state(OptionsState(self.mahjong))


class LobbyMenuState(MenuState):
    games = []

    def __init__(self, mahjong):
        self.games = get_lobby_games_list()

        items = [
            (_("Back"), self.on_back_pressed)
        ]
        for game in self.games:
            items.append(("%s - %s/4" % (game['title'], game['players']), lambda x: self.on_join_lobby_game(x, game)))
        items.append((_("Create new game"), self.on_create_new_game))
        MenuState.__init__(self, mahjong, _("Lobby"), items)
    
    def on_key_down(self, event):
        if event.key == pygame.K_ESCAPE:
            self.on_escape()
    
    def on_back_pressed(self, button):
        self.mahjong.open_main_menu()
    
    def on_escape(self):
        self.mahjong.open_main_menu()

    def on_join_lobby_game(self, button, game):
        print(game)
        self.mahjong.set_state(PlayerNameState(self.mahjong, lambda state: state.mahjong.set_state(ConnectingState(self.mahjong, bytearray(game['host'], encoding="utf-8"), game['port']))))

    def on_create_new_game(self, button):
        self.mahjong.set_state(GameTitleState(self.mahjong, lambda state, data: state.mahjong.set_state(ConnectingState(self.mahjong, data['host'], data['port']))))

class GameTitleState(EntryState):

    def __init__(self, mahjong, accept_callback):
        EntryState.__init__(self, mahjong, _(
            "Game title") + ":", "New game", 18)
        self.accept_callback = accept_callback

    def on_accept(self, string):
        result = create_new_game(string)
        if 'msg' in result:
            self.mahjong.set_state(ErrorState(self.mahjong, result['msg']))
            return
        self.accept_callback(self, {'host': result['host'], 'port': result['port']})

    def on_escape(self):
        self.mahjong.open_main_menu()

class OptionsState(MenuState):

    def __init__(self, mahjong):
        items = [(_("Toggle fullscreen"), self.on_fullscreen),
                 (_("Back to main menu"), self.on_back)]
        MenuState.__init__(self, mahjong, _("Options"), items)

    def on_back(self, button):
        self.mahjong.open_main_menu()

    def on_fullscreen(self, button):
        self.mahjong.toggle_fullscreen()
