# -*- coding: utf-8 -*-
"""
Lobby utils
"""
import requests
from config import Config

def get_lobby_games_list():
    response = requests.get("http://%s/lobby" % (Config.lobby_host)).json()
    return sorted(response.values(), key=lambda x: x['players'], reverse=True)[:5]

def create_new_game(title):
    response = requests.post("http://%s/lobby/new" % (Config.lobby_host), data={'title': title})
    return response.json()